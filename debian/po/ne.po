# translation of exim4_debian_po_ne.po to Nepali
# Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>, 2006.
# Shyam Krishna Bal <shyam@mpp.org.np>, 2006.
# shyam krishna bal <shyamkrishna_bal@yahoo.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: exim4_debian_po_ne\n"
"Report-Msgid-Bugs-To: pkg-exim4-maintainers@lists.alioth.debian.org\n"
"POT-Creation-Date: 2007-07-18 08:29+0200\n"
"PO-Revision-Date: 2007-07-25 18:20+0545\n"
"Last-Translator: shyam krishna bal <shyamkrishna_bal@yahoo.com>\n"
"Language-Team: Nepali <info@mpp.org.np>\n"
"Language: ne\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2;plural=(n!=1)\n"
"X-Generator: KBabel 1.11.4\n"

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
msgid "Remove undelivered messages in spool directory?"
msgstr "स्पूल डाइरेक्ट्रीमा हस्तान्तरण नभएका पत्रहरू हटाउनुहुन्छ ?"

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
msgid ""
"There are e-mail messages in the Exim spool directory /var/spool/exim4/"
"input/ which have not yet been delivered. Removing Exim will cause them to "
"remain undelivered until Exim is re-installed."
msgstr ""
"त्यहाँ एक्जिम स्पूल डाइरेक्ट्री /var/spool/exim4/input मा पत्रहरू छन् जुन अहिले सम्म "
"हस्तान्तरण भएको छैन । एक्जिम हटाउँनाले तिनीहरू एक्जिम पुन-स्थापना नभए सम्म हस्तान्तरण हुने "
"छैन ।"

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
msgid ""
"If this option is not chosen, the spool directory is kept, allowing the "
"messages in the queue to be delivered at a later date after Exim is re-"
"installed."
msgstr ""
"यदि यो विकल्प रोज्नु भएन भने, एक्जिम पुन: स्थापना गरेपछि पछिल्लो मितिमा हस्तान्तरण गर्न "
"लाममा सन्देशहरूलाई अनुमति दिएर, स्पूल डाइरेक्टरी राखिन्छ ।"

#. Type: error
#. Description
#: ../exim4-base.templates:2001 ../exim4-daemon-heavy.templates:1001
#: ../exim4-daemon-light.templates:1001 ../exim4.templates:1001
msgid "Reconfigure exim4-config instead of this package"
msgstr " यो प्याकेजहरूको सट्टामा एक्जिम४-कन्फिग पुन: कन्फिगर गर्नुहोस्"

#. Type: error
#. Description
#: ../exim4-base.templates:2001 ../exim4-daemon-heavy.templates:1001
#: ../exim4-daemon-light.templates:1001 ../exim4.templates:1001
msgid ""
"Exim4 has its configuration factored out into a dedicated package, exim4-"
"config. To reconfigure Exim4, use 'dpkg-reconfigure exim4-config'."
msgstr ""
"एक्जिम४ सँग समर्पित गरिएको प्याकेज, एक्जिम४-कन्फिगमा यसको कन्फिगरेसन फ्याक्टर्ड आउट छ ।"
"एक्जिम४ पुन: कन्फिगर गर्न, 'dpkg-reconfigure exim4-config' प्रयोग गर्नुहोस् ।"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "internet site; mail is sent and received directly using SMTP"
msgstr "इन्टरनेट साइट; पत्र पठाइयो र STMP प्रयोग गरेर सिधै प्राप्त गरियो"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "mail sent by smarthost; received via SMTP or fetchmail"
msgstr "स्मार्टहोस्टद्वारा पत्र पठाइयो; via SMTP वा fetchmail पायो"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "mail sent by smarthost; no local mail"
msgstr "स्मार्टहोस्टद्वारा पत्र पठाइयो; कुनै स्थानिय पत्र छैन"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "local delivery only; not on a network"
msgstr "स्थानिय हस्तान्तरण मात्र; सञ्जालमा होइन"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "no configuration at this time"
msgstr "यो समयमा कुनै कन्फिगरेसन भएन"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid "General type of mail configuration:"
msgstr "पत्र कन्फिगरेसनको सामन्य प्रकार:"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid ""
"Please select the mail server configuration type that best meets your needs."
msgstr "कन्फिगरेसन प्रकार चयन गर्नुहोस् जसले तपाईँको उत्तम आवश्यकताहरुलाई भेट्दछ ।"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid ""
"Systems with dynamic IP addresses, including dialup systems, should "
"generally be configured to send outgoing mail to another machine, called a "
"'smarthost' for delivery because many receiving systems on the Internet "
"block incoming mail from dynamic IP addresses as spam protection."
msgstr ""
"डायलअप प्रणालीहरू समावेश गरेर, गतिशिल आईपी ठेगानाहरूसँग प्रणालीहरू, अर्को मेशिनमा "
"निर्गमण पत्र पठाउन सामान्यतया कन्फिगर गरिनेछ, जसलाई हस्तान्तरणका लागि 'स्मार्टहोस्ट' "
"भनिन्छ किनभने इन्टरनेटमा धेरै प्राप्त गर्ने प्रणालीहरूले स्प्याम सुरक्षाको रूपमा गतिशिल आईपी "
"ठेगानाबाट आगमन पत्र ब्लक गर्दछ ।"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid ""
"A system with a dynamic IP address can receive its own mail, or local "
"delivery can be disabled entirely (except mail for root and postmaster)."
msgstr ""
"गतिशील आई पी ठेगाना भएको प्रणालीले यसको आफ्नै पत्र प्राप्त गर्न सक्छ, (मूल र "
"पोस्टमास्टरका लागि पत्र बाहेक) स्थानिय हस्तान्तरण पूर्ण रूपमा अक्षम गरिएको हुनसक्छ ।"

#. Type: boolean
#. Description
#: ../exim4-config.templates:2001
msgid "Really leave the mail system unconfigured?"
msgstr "साँच्चै कन्फिगर नभएको पत्र प्रणाली छोड्नुहुन्छ ?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:2001
msgid ""
"Until the mail system is configured, it will be broken and cannot be used. "
"Configuration at a later time can be done either by hand or by running 'dpkg-"
"reconfigure exim4-config' as root."
msgstr ""
"पत्र प्रणाली कन्फिगर नभएसम्म, यो विच्छेदन हुनेछ र प्रयोग हुन सक्दैन । पछिल्लो समयमा "
"कन्फिगरेसन या त हातद्वारा वा मूलको रूपमा 'dpkg-reconfigure exim4-config' चलाएर "
"गर्न सकिन्छ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid "System mail name:"
msgstr "प्रणाली पत्र नाम:"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"The 'mail name' is the domain name used to 'qualify' mail addresses without "
"a domain name."
msgstr ""
"'पत्र नाम' डोमेन नाम हो जुन डोमेन नाम बिना पत्र ठेगानाहरू 'योग्य' गर्न प्रयोग गरिन्छ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"This name will also be used by other programs. It should be the single, "
"fully qualified domain name (FQDN)."
msgstr ""
"यो नाम अन्य कार्यक्रमहरुले पनि प्रयोग गर्नेछ । यो एकल, पूर्ण योग्य डोमेन नाम (FQDN) पनि "
"हुन सक्छ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"Thus, if a mail address on the local host is foo@example.org, the correct "
"value for this option would be example.org."
msgstr ""
"तसर्थ, यदि स्थानिय होस्टमा पत्र ठेगाना foo@example.org छ भने,  यो विकल्पका लागि सही "
"मान example.org हुनेछ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"This name won't appear on From: lines of outgoing messages if rewriting is "
"enabled."
msgstr ""
"यदि तपाईँले पुन:लेखन सक्षम गर्नु भयो भने बाहिर जाने पत्रहरुको लाइनहरुबाट: यो नाम देखा पर्न "
"सक्दैन ।"

#. Type: string
#. Description
#: ../exim4-config.templates:4001
msgid "Other destinations for which mail is accepted:"
msgstr "पत्र स्वीकार गरिने अन्य गन्तब्य स्थानहरू:"

#. Type: string
#. Description
#: ../exim4-config.templates:4001
msgid ""
"Please enter a semicolon-separated list of recipient domains for which this "
"machine should consider itself the final destination. These domains are "
"commonly called 'local domains'. The local hostname (${fqdn}) and "
"'localhost' are always added to the list given here."
msgstr ""
"कृपया प्रापक डोमेनको अल्पबिराम-विभाजित सूची प्रविष्ट गर्नुहोस् जसका लागि यो मेशिन आफैले "
"अन्तिम गन्तब्य विचार गर्नेछ । यी डोमेनहरूलाई साझा रूपमा 'स्थानिय डोमेनहरू' भनिन्छ । "
"स्थानिय होस्टनाम (${fqdn}) र 'स्थानिय होस्ट' सधै यहाँ दिएको सूचीमा थप हुन्छ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:4001
msgid ""
"By default all local domains will be treated identically. If both a.example "
"and b.example are local domains, acc@a.example and acc@b.example will be "
"delivered to the same final destination. If different domain names should be "
"treated differently, it is necessary to edit the config files afterwards."
msgstr ""
"पूर्वनिर्धारण द्वारा सबै डोमेनहरू उस्तै व्यवहार गरिने छन् । यदि दुवै a.example र b.example "
"स्थानिय डोमेनहरू हुन भने, acc@a.example र acc@b.example एउटै अन्तिम गन्तब्यमा "
"हस्तान्तरण गरिनेछन् । यदि तपाईँ विभिन्न व्यवहार गर्न विभिन्न डोमेन नामहरू चाहनुहुन्छभने, "
"तपाईँले कनफिग फाइलहरू सम्पादन गर्नुपर्नेछ । "

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid "Domains to relay mail for:"
msgstr "पत्र प्रशारण गर्नको लागि डोमेनहरू:"

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid ""
"Please enter a semicolon-separated list of recipient domains for which this "
"system will relay mail, for example as a fallback MX or mail gateway. This "
"means that this system will accept mail for these domains from anywhere on "
"the Internet and deliver them according to local delivery rules."
msgstr ""
"कृपया प्रापक डोमेनको लागि अल्पबिराम-बिभाजित सूची प्रविष्ट गर्नुहोस् जसको लागि यो "
"प्रणालीले पत्र प्रसारण गर्दछ, जस्तै फल्ब्याक एम एक्स वा पत्र गेटवे । यसको मतलब यो प्रणालीले "
"इन्टरनेटमा कुनै ठाउँबाट यी डोमेनहरूका लागि पत्र स्वीकार गर्नेछ र तिनीहरूलाई स्थानिय "
"हस्तान्तरण नियमको आधारमा हस्तान्तरण गर्छ । "

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid "Do not mention local domains here. Wildcards may be used."
msgstr "यहाँ स्थानिय डोमेनहरू उल्लेख नगर्नुहोस् । वाइल्डकार्डहरू प्रयोग गरिएको हुनसक्छ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid "Machines to relay mail for:"
msgstr "पत्र प्रशारण गर्न मेशिनहरू:"

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid ""
"Please enter a semicolon-separated list of IP address ranges for which this "
"system will unconditionally relay mail, functioning as a smarthost."
msgstr ""
"कृपया आईपी ठेगाना दायराको अल्पबिराम-विभाजित सूची प्रविष्ट गर्नुहोस् जसका लागि यो "
"प्रणालीले स्मार्टहोस्टको रूपमा प्रकार्य गरेर, बिना सर्त पत्र प्रशारण गर्दछ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid ""
"You should use the standard address/prefix format (e.g. 194.222.242.0/24 or "
"5f03:1200:836f::/48)."
msgstr ""
"तपाईँले मानक ठेगाना/उपसर्ग ढाँचा (e.g. 194.222.242.0/24 वा 5f03:1200:836f::/48) "
"प्रयोग गर्नु पर्नेछ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid ""
"If this system should not be a smarthost for any other host, leave this list "
"blank."
msgstr ""
"यदि यो प्रणाली अन्य कुनै होस्टको लागि स्मार्टहोस्ट बनेन भने, यो सूची खाली छोड्नुहोस् ।"

#. Type: string
#. Description
#: ../exim4-config.templates:7001
msgid "Visible domain name for local users:"
msgstr "स्थानिय प्रयोगकर्ताहरुको लागि दृश्यात्मक डोमेन नाम:"

#. Type: string
#. Description
#: ../exim4-config.templates:7001
msgid ""
"The option to hide the local mail name in outgoing mail was enabled. It is "
"therefore necessary to specify the domain name this system should use for "
"the domain part of local users' sender addresses."
msgstr ""
"निर्गामन पत्रमा स्थानिय पत्र नाम लुकाउन यो विकल्प सक्षम भयो । डोमेन नाम निर्दिष्ट गर्न "
"यो यसकारण आवश्यक भयो कि यो प्रणाली स्थानिय प्रयोगकर्ताको, प्रेषक ठेगानाहरूको डोमेन "
"भागका लागि प्रयोग हुनेछ । "

#. Type: string
#. Description
#: ../exim4-config.templates:8001
msgid "IP address or host name of the outgoing smarthost:"
msgstr "निर्गामन स्मार्टहोस्टको आई पी ठेगाना वा होस्टनाम:"

#. Type: string
#. Description
#: ../exim4-config.templates:8001
msgid ""
"Please enter the IP address or the host name of a mail server that this "
"system should use as outgoing smarthost. If the smarthost only accepts your "
"mail on a port different from TCP/25, append two colons and the port number "
"(for example smarthost.example::587 or 192.168.254.254::2525). Colons in "
"IPv6 addresses need to be doubled."
msgstr ""
"कृपया पत्र सर्भरको आई पी ठेगाना वा होस्ट नाम प्रविष्ट गर्नुहोस् ता कि यो प्रणाली "
"निर्गामन स्मार्टहोस्टको रूपमा प्रयोग हुनेछ । यदि स्मार्टहोस्टले TCP/25 बाट विभिन्न पोर्टमा "
"तपाईँको पत्र मात्र स्वीकार गर्दछ भने, दुईवटा अल्पबिरामहरू र पोर्ट नम्बर (जस्तै smarthost."
"example::587 वा 192.168.254.254::2525) थप्नुहोस् । IPv6 ठेगानाहरूमा अल्पबिरामहरू "
"डबल हुन आवश्यक छ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:8001
msgid ""
"If the smarthost requires authentication, please refer to the Debian-"
"specific README files in /usr/share/doc/exim4-base for notes about setting "
"up SMTP authentication."
msgstr ""
"यदि स्मार्टहोस्टलाई आधिकारिकताको आवश्यक परेमा, SMTP आधिकारिकता सेटअप गर्ने बारे "
"द्रष्टब्यको लागि /usr/share/doc/exim4-base/README.Debian.gz सन्दर्भ गर्नुहोस् ।"

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid "Root and postmaster mail recipient:"
msgstr "मूल र पोस्टमास्टर पत्र प्रापक:"

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid ""
"Mail for the 'postmaster', 'root', and other system accounts needs to be "
"redirected to the user account of the actual system administrator."
msgstr ""
"'पोस्टमास्टर', 'मूल', र अन्य प्रणाली खाताहरू वास्तविक प्रणाली प्रशासकको प्रयोगकर्ता "
"खातामा पुन: निर्देशित हुन आवश्यक हुन्छ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid ""
"If this value is left empty, such mail will be saved in /var/mail/mail, "
"which is not recommended."
msgstr ""
"यदि यो मान खाली राखियो भने, सो पत्र var/mail/mail मा बचत गरिनेछ जुन सिफारिस "
"गरिएको छैन । "

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid ""
"Note that postmaster's mail should be read on the system to which it is "
"directed, rather than being forwarded elsewhere, so (at least one of) the "
"users listed here should not redirect their mail off this machine. A 'real-' "
"prefix can be used to force local delivery."
msgstr ""
"याद गर्नुहोस् पोस्टमास्टरको पत्र यताउती पठाइनु भन्दा, यसले निर्देशित गरेको प्रणालीमा "
"पढिनुपर्छ, त्यसैले यहाँ सूचीकृत भएको प्रयोगकर्ता मध्ये (कम्तिमा एकजना) ले यो मेशिनमा "
"तिनीहरूको पत्र पुन: निर्देशन गर्ने छैन । स्थानिय हस्तान्तरणलाई जोर गर्न 'वास्तविक' उपसर्ग "
"प्रयोग गर्न सकिन्छ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid "Multiple user names need to be separated by spaces."
msgstr "बहुँविध प्रयोगकर्ता नामहरू खाली ठाउँद्वारा विभाजित हुन आवश्यक छ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid "IP-addresses to listen on for incoming SMTP connections:"
msgstr "SMTP जडानहरुको आगमनको लागि सुन्न IP-ठेगानाहरू:"

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid ""
"Please enter a semicolon-separated list of IP addresses. The Exim SMTP "
"listener daemon will listen on all IP addresses listed here."
msgstr ""
"कृपया आई पी ठेगानाको अल्पबिराम-विभाजित सूची प्रविष्ट गर्नुहोस् । एक्जिम SMTP श्रोता "
"डेयमनले यहाँ सूचिकृत सबै आई पी ठेगानाहरूमा सुन्नेछ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid ""
"An empty value will cause Exim to listen for connections on all available "
"network interfaces."
msgstr ""
"एउटा खाली मानले सबै उपलब्ध सञ्जाल अन्तरमोहडाहरूमा जडानहरूका लागि एक्जिम सुन्न सक्ने "
"बनाउँन सक्छ ।"

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid ""
"If this system only receives mail directly from local services (and not from "
"other hosts), it is suggested to prohibit external connections to the local "
"Exim daemon. Such services include e-mail programs (MUAs) which talk to "
"localhost only as well as fetchmail. External connections are impossible "
"when 127.0.0.1 is entered here, as this will disable listening on public "
"network interfaces."
msgstr ""
"यदि यो प्रणालीले स्थानिय सेवाहरू जस्तै फेचमेल वा स्थानिय होस्टमा बोलिरहेको तपाईँको (MUA) "
"इमेल कार्यक्रमबाट (र अन्य होस्टहरूबाट होइन) मात्र प्रत्यक्ष रूपमा इमेल प्राप्त गर्दैन भने, "
"स्थानिय एक्जिममा बाह्य जडानहरू निषेध गर्न आवश्यक छ । यसलाई यहाँ १२७.०.०.१ प्रविष्ट गरेर "
"पूरा गर्न सकिन्छ । यसले सार्वजनिक सञ्जाल इन्टरफेसहरूमा सुन्नलाई अक्षम गरिदिन्छ ।"

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid "Keep number of DNS-queries minimal (Dial-on-Demand)?"
msgstr "डी एन एस-क्वेरीहरूको मिनिमल (डायल-अन-डिमान्ड) को सङ्ख्या राख्नुहुन्छ ?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid ""
"In normal mode of operation Exim does DNS lookups at startup, and when "
"receiving or delivering messages. This is for logging purposes and allows "
"keeping down the number of hard-coded values in the configuration."
msgstr ""
"सञ्चालनको सामान्य मोडमा एक्जिमले सुरुआतमा, र सन्देशहरू प्राप्त गर्दा र हस्तान्तरण गर्दामा "
"डी एन एस लाई देखाउँछ । यो लगइन उद्देश्यहरूको लागि हो र कन्फिगरेसनमा हार्ड-कोडेड "
"मानहरूको सङ्ख्या तल राख्न अनुमति दिन्छ ।"

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid ""
"If this system does not have a DNS full service resolver available at all "
"times (for example if its Internet access is a dial-up line using dial-on-"
"demand), this might have unwanted consequences. For example, starting up "
"Exim or running the queue (even with no messages waiting) might trigger a "
"costly dial-up-event."
msgstr ""
"यदि यो प्रणालीसँग सबै समयमा डि एन एस पूरा सेवा पुन: समाधानकर्ता उपलब्ध छैन भने (जस्तै "
"यदि यसको इन्टरनेट पहुँच डायल-अन-डिमान्ड प्रयोग गरिरहेको डायल-अप लाइन हो भने) यो संग "
"नचाहेको परिणाम हुनुपर्छ । उदाहरणको लागि, एक्जिम सुरु गरेर वा (कुनै सन्देशहरू पनि प्रतिक्षा "
"नगरिरहेको) लाम सञ्चालन गरेर महंगो dial-up-event ट्रिगर गर्न सकिदैन ।"

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid ""
"This option should be selected if this system is using Dial-on-Demand. If it "
"has always-on Internet access, this option should be disabled."
msgstr ""
"यदि यो प्रणालीले डायल-अन-डिमान्ड प्रयोग गरिरहेको छ भने यो विकल्प चयन हुनेछ । यदि यो "
"सँग इन्टरनेट पहुँच सधै खुल्ला हुन्छ भने, यो विकल्प अक्षम गरिनेछ ।"

#. Type: title
#. Description
#: ../exim4-config.templates:12001
msgid "Mail Server configuration"
msgstr "पत्र सर्भर कन्फिगरेसन"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid "Split configuration into small files?"
msgstr "सानो फाइलहरुमा कन्फिगरेसन विभाजन गर्नुहुन्छ ?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid ""
"The Debian exim4 packages can either use 'unsplit configuration', a single "
"monolithic file (/etc/exim4/exim4.conf.template) or 'split configuration', "
"where the actual Exim configuration files are built from about 50 smaller "
"files in /etc/exim4/conf.d/."
msgstr ""
"डेवियन एक्जिम४ प्याकेजहरूले ले या त 'अविभाजित कन्फिगरेसन', वा एकल monolithic फाइल (/"
"etc/exim4/exim4.conf.template) वा 'विभाजित कन्फिगरेसन' प्रयोग गर्न सक्छ, जहाँ "
"वास्तविक एक्जिम कन्फिगरेसन फाइलहरू /etc/exim4/conf.d मा ५० वटा सानो फाइलहरू मिलेर "
"निर्माण भएको हुन्छ ।"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid ""
"Unsplit configuration is better suited for large modifications and is "
"generally more stable, whereas split configuration offers a comfortable way "
"to make smaller modifications but is more fragile and might break if "
"modified carelessly."
msgstr ""
"अविभाजित कन्फिगरेसन ठूलो परिमार्जनहरुको लागि अति उत्तम छ र सामन्य रूपमा धेरै स्थिर पनि "
"छ, जबकि विभाजित कन्फिगरेसनले सानो परिमार्जनहरु बनाउन सुविधाजनक बाटोको प्रस्ताव गर्दछ "
"तर धेरै मुलायम हुने भएकोले ठूलो रुपमा परिमार्जन हुँदा विच्छेदन हुन सक्छ ।"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid ""
"A more detailed discussion of split and unsplit configuration can be found "
"in the Debian-specific README files in /usr/share/doc/exim4-base."
msgstr ""
"विभाजित र अविभाजित कन्फिगरेसनको बारे धेरै विस्तृत छलफल /usr/share/doc/exim4-base/"
"README.Debian.gz मा फेला पार्न सकिन्छ । "

#. Type: boolean
#. Description
#: ../exim4-config.templates:14001
msgid "Hide local mail name in outgoing mail?"
msgstr "बाहिर जाने पत्रमा स्थानिय पत्र नाम लुकाउनुहुन्छ ?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:14001
msgid ""
"The headers of outgoing mail can be rewritten to make it appear to have been "
"generated on a different system. If this option is chosen, '${mailname}', "
"'localhost' and '${dc_other_hostnames}' in From, Reply-To, Sender and Return-"
"Path are rewritten."
msgstr ""
"निर्गामन पत्रको हेडरहरू विभिन्न प्रणालीमा सिर्जना गरेर देखाउन पुन: लेखन गर्न सकिन्छ । यदि "
"यो विकल्प रोजियो भने, '${mailname}', 'localhost' र '${dc_other_hostnames}' "
"बाटमा, लाई-जवाफ, प्रेषक र फर्कने-बाटो पुन: लेखन गरिन्छ ।"

#. Type: select
#. Choices
#: ../exim4-config.templates:15001
msgid "mbox format in /var/mail/"
msgstr "/var/mail मा पत्रमञ्जूषा ढाँचा "

#. Type: select
#. Choices
#: ../exim4-config.templates:15001
msgid "Maildir format in home directory"
msgstr "गृह डाइरेक्टरीमा पत्र डाइरेक्टरी ढाँचा"

#. Type: select
#. Description
#: ../exim4-config.templates:15002
msgid "Delivery method for local mail:"
msgstr "स्थानिय पत्रको लागि हस्तान्तरण विधि:"

#. Type: select
#. Description
#: ../exim4-config.templates:15002
msgid ""
"Exim is able to store locally delivered email in different formats. The most "
"commonly used ones are mbox and Maildir. mbox uses a single file for the "
"complete mail folder stored in /var/mail/. With Maildir format every single "
"message is stored in a separate file in ~/Maildir/."
msgstr ""
"एक्जिम विभिन्न ढाँचाहरूमा स्थानिय रूपमा हस्तान्तरण गरिएको इमेल भण्डारण गर्न सक्षम भयो ।"
"प्रायजसो धेरै प्रयोग गरिएको ढाँचाहरू पत्रमञ्जूषा र पत्र डाइरेक्टरी हो । पत्रमञ्जूषाले /var/"
"mail/ मा भण्डारण गरिएको पूरै पत्र फोल्डरको लागि एकल फाइल प्रयोग गर्दछ । पत्र "
"डाइरेकटरी ढाँचाद्वारा हरेक एकल सन्देश ~/Maildir/ मा भिन्नै फाइलमा भण्डारण गरिएको हुन्छ ।"

#. Type: select
#. Description
#: ../exim4-config.templates:15002
msgid ""
"Please note that most mail tools in Debian expect the local delivery method "
"to be mbox in their default."
msgstr ""
"कृपया याद गर्नुहोस् कि डेवियनमा धेरै पत्र उपकरणहरूले तिनीहरूको पूर्वनिर्धारणमा पत्रमञ्जूषा हुन "
"स्थानिय हस्तान्तरण विधिको अपेक्षा गरेको हुन्छ ।"
