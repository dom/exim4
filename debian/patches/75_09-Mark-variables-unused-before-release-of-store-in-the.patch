From 1f3a53b0af67aac909b2ba6ce1244a8a64d9a718 Mon Sep 17 00:00:00 2001
From: Jeremy Harris <jgh146exb@wizmail.org>
Date: Tue, 13 Mar 2018 16:27:54 +0000
Subject: [PATCH 3/4] Mark variables unused before release of store in the
 queue-runner loop

---
 doc/ChangeLog |  2 ++
 src/functions.h   |  5 +--
 src/queue.c       |  4 +--
 src/spool_in.c    | 89 +++++++++++++++++++++++++++++----------------------
 4 files changed, 56 insertions(+), 44 deletions(-)

diff --git a/doc/ChangeLog b/doc/ChangeLog
index e7de5731..1de81962 100644
--- a/doc/ChangeLog
+++ b/doc/ChangeLog
@@ -90,6 +90,8 @@ JH/28 Ensure that variables possibly set during message acceptance are marked
       dead before release of memory in the daemon loop.  This stops complaints
       about them when the debug_store option is enabled.  Discovered specifically
       for sender_rate_period, but applies to a whole set of variables.
+      Do the same for the queue-runner loop, for variables set from spool
+      message files.
 
 
 Exim version 4.90
diff --git a/src/functions.h b/src/functions.h
index ccd3d581..dfe8ff57 100644
--- a/src/functions.h
+++ b/src/functions.h
@@ -430,13 +430,14 @@ extern int     smtp_write_command(smtp_outblock *, int, const char *, ...) PRINT
 extern int     spam(const uschar **);
 extern FILE   *spool_mbox(unsigned long *, const uschar *, uschar **);
 #endif
-extern BOOL    spool_move_message(uschar *, uschar *, uschar *, uschar *);
+extern void    spool_clear_header_globals(void);
 extern uschar *spool_dname(const uschar *, uschar *);
 extern uschar *spool_fname(const uschar *, const uschar *, const uschar *, const uschar *);
-extern uschar *spool_sname(const uschar *, uschar *);
+extern BOOL    spool_move_message(uschar *, uschar *, uschar *, uschar *);
 extern int     spool_open_datafile(uschar *);
 extern int     spool_open_temp(uschar *);
 extern int     spool_read_header(uschar *, BOOL, BOOL);
+extern uschar *spool_sname(const uschar *, uschar *);
 extern int     spool_write_header(uschar *, int, uschar **);
 extern int     stdin_getc(unsigned);
 extern int     stdin_feof(void);
diff --git a/src/queue.c b/src/queue.c
index 09a149e9..e613bfac 100644
--- a/src/queue.c
+++ b/src/queue.c
@@ -601,9 +601,7 @@ for (i = queue_run_in_order ? -1 : 0;
 
       /* Recover store used when reading the header */
 
-      received_protocol = NULL;
-      sender_address = sender_ident = NULL;
-      authenticated_id = authenticated_sender = NULL;
+      spool_clear_header_globals();
       store_reset(reset_point2);
       if (!wanted) continue;      /* With next message */
       }
diff --git a/src/spool_in.c b/src/spool_in.c
index 2a99c63d..d4db37de 100644
--- a/src/spool_in.c
+++ b/src/spool_in.c
@@ -206,49 +206,13 @@ return TRUE;
 
 
 
-/*************************************************
-*             Read spool header file             *
-*************************************************/
-
-/* This function reads a spool header file and places the data into the
-appropriate global variables. The header portion is always read, but header
-structures are built only if read_headers is set true. It isn't, for example,
-while generating -bp output.
-
-It may be possible for blocks of nulls (binary zeroes) to get written on the
-end of a file if there is a system crash during writing. It was observed on an
-earlier version of Exim that omitted to fsync() the files - this is thought to
-have been the cause of that incident, but in any case, this code must be robust
-against such an event, and if such a file is encountered, it must be treated as
-malformed.
-
-As called from deliver_message() (at least) we are running as root.
-
-Arguments:
-  name          name of the header file, including the -H
-  read_headers  TRUE if in-store header structures are to be built
-  subdir_set    TRUE is message_subdir is already set
-
-Returns:        spool_read_OK        success
-                spool_read_notopen   open failed
-                spool_read_enverror  error in the envelope portion
-                spool_read_hdrerror  error in the header portion
-*/
-
-int
-spool_read_header(uschar *name, BOOL read_headers, BOOL subdir_set)
-{
-FILE *f = NULL;
-int n;
-int rcount = 0;
-long int uid, gid;
-BOOL inheader = FALSE;
-uschar *p;
-
 /* Reset all the global variables to their default values. However, there is
 one exception. DO NOT change the default value of dont_deliver, because it may
 be forced by an external setting. */
 
+void
+spool_clear_header_globals(void)
+{
 acl_var_c = acl_var_m = NULL;
 authenticated_id = NULL;
 authenticated_sender = NULL;
@@ -328,6 +292,53 @@ message_utf8_downconvert = 0;
 
 dsn_ret = 0;
 dsn_envid = NULL;
+}
+
+
+/*************************************************
+*             Read spool header file             *
+*************************************************/
+
+/* This function reads a spool header file and places the data into the
+appropriate global variables. The header portion is always read, but header
+structures are built only if read_headers is set true. It isn't, for example,
+while generating -bp output.
+
+It may be possible for blocks of nulls (binary zeroes) to get written on the
+end of a file if there is a system crash during writing. It was observed on an
+earlier version of Exim that omitted to fsync() the files - this is thought to
+have been the cause of that incident, but in any case, this code must be robust
+against such an event, and if such a file is encountered, it must be treated as
+malformed.
+
+As called from deliver_message() (at least) we are running as root.
+
+Arguments:
+  name          name of the header file, including the -H
+  read_headers  TRUE if in-store header structures are to be built
+  subdir_set    TRUE is message_subdir is already set
+
+Returns:        spool_read_OK        success
+                spool_read_notopen   open failed
+                spool_read_enverror  error in the envelope portion
+                spool_read_hdrerror  error in the header portion
+*/
+
+int
+spool_read_header(uschar *name, BOOL read_headers, BOOL subdir_set)
+{
+FILE *f = NULL;
+int n;
+int rcount = 0;
+long int uid, gid;
+BOOL inheader = FALSE;
+uschar *p;
+
+/* Reset all the global variables to their default values. However, there is
+one exception. DO NOT change the default value of dont_deliver, because it may
+be forced by an external setting. */
+
+spool_clear_header_globals();
 
 /* Generate the full name and open the file. If message_subdir is already
 set, just look in the given directory. Otherwise, look in both the split
-- 
2.16.2

